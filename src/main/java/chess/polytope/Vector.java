package chess.polytope;
//
import java.util.Arrays;
//
final class Vector
{
    private final double[] scalars;
    //
    public Vector(int scalarCount)
    {
        super();
        //
        scalars = new double[scalarCount];
    }
    //
    public Vector(double[] scalars)
    {
        super();
        //
        this.scalars = scalars;
    }
    //
    public Vector plus(int index, double delta)
    {
        double[] result = new double[scalars.length];
        System.arraycopy(scalars, 0, result, 0, scalars.length);
        result[index] = scalars[index] + delta;
        //
        return new Vector(result);
    }
    //
    public Vector plus(Vector other)
    {
        double[] result = new double[scalars.length];
        //
        for (int i = 0; i < scalars.length; i++)
        {
            result[i] = scalars[i] + other.scalars[i];
        }
        //
        return new Vector(result);
    }
    //
    public Vector times(double factor)
    {
        double[] result = new double[scalars.length];
        //
        for (int i = 0; i < scalars.length; i++)
        {
            result[i] = scalars[i] * factor;
        }
        //
        return new Vector(result);
    }
    //
    public double get(int index)
    {
        return scalars[index];
    }
    //
    public int size()
    {
        return scalars.length;
    }
    //
    @Override
    public String toString()
    {
        return Arrays.toString(scalars);
    }
}
