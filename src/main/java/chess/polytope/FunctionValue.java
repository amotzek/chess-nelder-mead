package chess.polytope;
//
final class FunctionValue implements Comparable<FunctionValue>
{
    private final Vector point;
    private double value;
    //
    public FunctionValue(Vector point, double value)
    {
        super();
        //
        this.point = point;
        this.value = value;
    }
    //
    public FunctionValue(Vector point)
    {
        this(point, 0d);
    }
    //
    public Vector getPoint()
    {
        return point;
    }
    //
    public double getValue()
    {
        return value;
    }
    //
    public void adjustValue(double delta)
    {
        value += delta;
    }
    //
    @Override
    public int compareTo(FunctionValue other)
    {
        return Double.compare(other.value, value); // größter Function Value zuerst
    }
    //
    @Override
    public boolean equals(Object object)
    {
        if (object instanceof FunctionValue)
        {
            FunctionValue other = (FunctionValue) object;
            //
            return point == other.point;
        }
        //
        return false;
    }
    //
    @Override
    public int hashCode()
    {
        return point.hashCode();
    }
    //
    @Override
    public String toString()
    {
        return "FunctionValue{" +
                "point=" + point +
                ", value=" + value +
                '}';
    }
}
