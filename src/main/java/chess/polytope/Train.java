package chess.polytope;
//
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
//
public class Train
{
    public static void main(String[] args)
    {
        long tries = getTries(args);
        setLogLevel(getQuiet(args) ? Level.INFO : Level.FINE);
        Logger logger = Logger.getLogger("chess.polytope.Train");
        logger.info(String.format("start optimization with boards %s", tries));
        var evaluator = new ChessEvaluator(tries);
        var optimization = new PolytopeAscent(new Vector(new double[] { 15d, 8d, 12d }), evaluator);
        var point = optimization.findMaximum(1000d);
        logger.info(String.format("optimum is %s", point));
    }
    //
    private static long getTries(String[] args)
    {
        for (var arg : args)
        {
            if (arg.startsWith("--boards=")) return Long.parseLong(arg.substring(9));
        }
        //
        return 1500000L;
    }
    //
    private static boolean getQuiet(String[] args)
    {
        for (var arg : args)
        {
            if ("--quiet".equals(arg)) return true;
        }
        //
        return false;
    }
    //
    private static void setLogLevel(Level level)
    {
        Logger root = Logger.getLogger("");
        root.setLevel(level);
        //
        for (Handler handler : root.getHandlers())
        {
            handler.setLevel(level);
        }
    }
}
