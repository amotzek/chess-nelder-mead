package chess.polytope;
//
import java.util.Collection;
import java.util.List;
//
public interface Evaluator
{
    List<FunctionValue> evaluate(Collection<Vector> points);
}
