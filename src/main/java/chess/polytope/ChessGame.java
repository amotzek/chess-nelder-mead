package chess.polytope;
//
import chess.board.Board;
import chess.board.Color;
import chess.search.ChessMiniMaxSearch;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//
final class ChessGame
{
    private static final double WEIGHT9 = 5000d;
    private static final double[] PIECE_WEIGHTS = new double[] { 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, WEIGHT9 };
    //
    private static final Logger logger = Logger.getLogger("chess.polytope.ChessGame");
    //
    private final FunctionValue player1;
    private final FunctionValue player2;
    private final long tries;
    private double result1;
    private double result2;
    //
    public ChessGame(FunctionValue player1, FunctionValue player2, long tries)
    {
        super();
        //
        this.player1 = player1;
        this.player2 = player2;
        this.tries = tries;
    }
    //
    private static double[] getWeights(FunctionValue player)
    {
        Vector point = player.getPoint();
        //
        return new double[] { 8d, // Doppelbauern
                              2d, // Promotion der Bauern (Endspiel)
                              1d, // Zentralität der Bauern (Eröffnung)
                              1d, // Zentralität der Damen und Könige (Endspiel)
                              point.get(0), // Zentralität der Springer, Läufer und Türme
                              point.get(1), // Königsangriffe
                              point.get(2), // Königsverteidigung
                              80d, // Rochaden (Eröffnung)
                              WEIGHT9 }; // Figurenwerte
    }
    //
    private ChessMiniMaxSearch createSearch(Board board)
    {
        return new ChessMiniMaxSearch(board, getWeights(board.getColor() == Color.WHITE ? player1 : player2));
    }
    //
    private double predictResult(Board board)
    {
        double staticValue = board.getStaticValue(0, PIECE_WEIGHTS) / WEIGHT9;
        Color boardColor = board.getColor();
        Color winColor = null;
        //
        if (staticValue > 0.0) winColor = boardColor;
        //
        if (staticValue < 0.0) winColor = Color.getOpposite(boardColor);
        //
        if (winColor == null) return 0d;
        //
        staticValue = Math.abs(staticValue);
        //
        return Color.WHITE.equals(winColor) ? staticValue : -staticValue;
    }
    //
    private void sumUpPredictedResults(double predictedResult)
    {
        if (predictedResult > 0d)
        {
            result1 += predictedResult;
        }
        else
        {
            result2 += predictedResult;
        }
    }
    //
    private static Board makeMove(Board board, String moveString)
    {
        var queue = board.generateMoves();
        //
        while (!queue.isEmpty())
        {
            var move = queue.poll();
            //
            if (moveString.equals(move.toString())) return board.applyMove(move);
        }
        //
        throw new IllegalArgumentException(moveString + " is not a valid move");
    }
    //
    public void play()
    {
        var board = new Board();
        // Sizilianische Verteidigung
        board = makeMove(board, "e2-e4");
        board = makeMove(board, "c7-c5");
        board = makeMove(board, "Sg1-f3");
        board = makeMove(board, "d7-d6");
        board = makeMove(board, "d2-d4");
        play(board);
        // Abgelehntes Damengambit
        board = new Board();
        board = makeMove(board, "d2-d4");
        board = makeMove(board, "d7-d5");
        board = makeMove(board, "c2-c4");
        board = makeMove(board, "e7-e6");
        play(board);
    }
    //
    private void play(Board board)
    {
        while (true)
        {
            var search = createSearch(board);
            long then = System.currentTimeMillis();
            var path = search.findBestPath(tries);
            long now = System.currentTimeMillis();
            double predictedResult = predictResult(board);
            sumUpPredictedResults(predictedResult);
            log(board, search, 1L + now - then, path, predictedResult);
            //
            if (path.size() < 2) break;
            //
            board = path.get(1);
        }
    }
    //
    public void adjustFunctionValues()
    {
        player1.adjustValue(result1);
        player2.adjustValue(- result2);
    }
    //
    private void log(Board board, ChessMiniMaxSearch search, long elapsed, List<Board> path, double predictedResult)
    {
        if (logger.isLoggable(Level.FINE))
        {
            double branchingFactor = search.getAverageBranchingFactor();
            long leafCount = search.getLeafCount();
            long performance = (1000L * leafCount) / elapsed;
            long transpositionCount = search.getTranspositionCount();
            var predictedResultFormat = new DecimalFormat("#0");
            var branchingFactorFormat = new DecimalFormat("#0.00");
            var messageBuilder = new StringBuilder();
            messageBuilder.append(board.getColor());
            messageBuilder.append(' ');
            messageBuilder.append(predictedResultFormat.format(predictedResult));
            messageBuilder.append(' ');
            messageBuilder.append(branchingFactorFormat.format(branchingFactor));
            messageBuilder.append(' ');
            messageBuilder.append(performance);
            messageBuilder.append(' ');
            messageBuilder.append(transpositionCount);
            //
            for (var element : path)
            {
                var move = element.getBeforeMove();
                messageBuilder.append(" ");
                messageBuilder.append(move);
            }
            //
            logger.fine(messageBuilder.toString());
        }
    }
}
