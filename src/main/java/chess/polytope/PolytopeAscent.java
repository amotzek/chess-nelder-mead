package chess.polytope;
//
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
//
public final class PolytopeAscent
{
    private static final double ALPHA = 1d;
    private static final double GAMMA = 2d;
    private static final double RHO = 0.5d;
    private static final double SIGMA = 0.5d;
    //
    private static final Logger logger = Logger.getLogger("chess.polytope.PolytopeAscent");
    //
    private final Vector startPoint;
    private final Evaluator evaluator;
    //
    public PolytopeAscent(Vector startPoint, Evaluator evaluator)
    {
        super();
        //
        if (startPoint.size() < 2) throw new IllegalArgumentException("size of start point is less than 2");
        //
        this.startPoint = startPoint;
        this.evaluator = evaluator;
    }
    //
    public Vector findMaximum(double limit)
    {
        var polytope = createPolytope(startPoint);
        //
        while (true)
        {
            var functionValues = evaluate(polytope);
            //
            for (var functionValue : functionValues)
            {
                logger.info(functionValue.toString());
            }
            //
            if (getValueRange(functionValues) < limit) return getBest(functionValues).getPoint();
            //
            var points = getPoints(functionValues);
            //
            var centeroid = getCenteroid(points);
            var reflection = getReflection(centeroid, points);
            var expansion = getExpansion(centeroid, reflection);
            var contraction = getContraction(centeroid, points);
            //
            points.add(reflection);
            points.add(expansion);
            points.add(contraction);
            //
            functionValues = evaluate(points);
            polytope = selectPolytope(functionValues, points, reflection, expansion, contraction);
        }
    }
    //
    private static Collection<Vector> createPolytope(Vector point)
    {
        var points = new ArrayList<Vector>();
        points.add(point);
        //
        for (int i = 0; i < point.size(); i++)
        {
            double coordinate = Math.abs(point.get(i));
            double delta = coordinate == 0d ? 1d : Math.signum(coordinate) * Math.max(1d, 0.2d * coordinate);
            points.add(point.plus(i, delta));
        }
        //
        return points;
    }
    //
    private List<FunctionValue> evaluate(Collection<Vector> points)
    {
        return evaluator.evaluate(points);
    }
    //
    private double getValueRange(List<FunctionValue> functionValues)
    {
        var best = getBest(functionValues);
        var worst = getWorst(functionValues);
        //
        return best.getValue() - worst.getValue();
    }
    //
    private static List<Vector> getPoints(List<FunctionValue> functionValues)
    {
        var points = new LinkedList<Vector>();
        //
        for (var functionValue : functionValues)
        {
            points.addLast(functionValue.getPoint());
        }
        //
        return points;
    }
    //
    private Vector getCenteroid(List<Vector> points)
    {
        int pointCount = 0;
        var centeroid = new Vector(startPoint.size());
        //
        for (var point : queryButWorst(points))
        {
            centeroid = centeroid.plus(point);
            pointCount++;
        }
        //
        assert pointCount >= 1;
        //
        return centeroid.times(1d / pointCount);
    }
    //
    private Vector getReflection(Vector centeroid, List<Vector> points)
    {
        var worst = getWorst(points);
        var reflection = centeroid.plus(centeroid.times(ALPHA));
        reflection = reflection.plus(worst.times(-ALPHA));
        //
        return reflection;
    }
    //
    private static Vector getExpansion(Vector centeroid, Vector reflection)
    {
        var expansion = centeroid.plus(reflection.times(GAMMA));
        expansion = expansion.plus(centeroid.times(-GAMMA));
        //
        return expansion;
    }
    //
    private Vector getContraction(Vector centeroid, List<Vector> points)
    {
        var worst = getWorst(points);
        var contraction = centeroid.plus(worst.times(RHO));
        contraction = contraction.plus(centeroid.times(-RHO));
        //
        return contraction;
    }
    //
    private List<Vector> createShrink(List<Vector> points, Vector best)
    {
        var shrink = new ArrayList<Vector>(startPoint.size() + 1);
        //
        for (var point : queryButBest(points))
        {
            point = best.plus(point.times(SIGMA));
            point = point.plus(best.times(-SIGMA));
            shrink.add(point);
        }
        //
        return shrink;
    }
    //
    private List<Vector> selectPolytope(List<FunctionValue> functionValues, List<Vector> points, Vector reflection, Vector expansion, Vector contraction)
    {
        List<Vector> polytope = null;
        var best = getBest(points);
        var worst = getWorst(points);
        var secondWorst = getSecondWorst(points);
        //
        if (hasGreaterValue(functionValues, reflection, secondWorst) && !hasGreaterValue(functionValues, reflection, best))
        {
            polytope = queryButWorst(points);
            polytope.add(reflection);
            logger.info(String.format("replaced %s by reflection %s", worst, reflection));
        }
        else if (hasGreaterValue(functionValues, reflection, best))
        {
            if (hasGreaterValue(functionValues, expansion, reflection))
            {
                polytope = queryButWorst(points);
                polytope.add(expansion);
                logger.info(String.format("replaced %s by expansion %s", worst, expansion));
            }
            else
            {
                polytope = queryButWorst(points);
                polytope.add(reflection);
                logger.info(String.format("replaced %s by reflection %s", worst, reflection));
            }
        }
        else if (hasGreaterValue(functionValues, contraction, worst))
        {
            polytope = queryButWorst(points);
            polytope.add(contraction);
            logger.info(String.format("replaced %s by contraction %s", worst, contraction));
        }
        else
        {
            polytope = createShrink(points, best);
            polytope.add(best);
            logger.info("shrank");
        }
        //
        assert polytope.size() == startPoint.size() + 1;
        //
        return polytope;
    }
    //
    private static int getIndex(List<FunctionValue> functionValues, Vector point)
    {
        int index = 0;
        //
        for (var functionValue : functionValues)
        {
            if (functionValue.getPoint() == point) return index;
            //
            index++;
        }
        //
        throw new IllegalArgumentException("vector is not evaluated");
    }
    //
    private static boolean hasGreaterValue(List<FunctionValue> functionValues, Vector vector1, Vector vector2)
    {
        return getIndex(functionValues, vector1) < getIndex(functionValues, vector2);
    }
    //
    private <T> List<T> queryButWorst(List<T> source)
    {
        var destination = new ArrayList<T>(startPoint.size() + 1);
        //
        for (int i = 0; i < startPoint.size(); i++)
        {
            destination.add(source.get(i));
        }
        //
        return destination;
    }
    //
    private <T> List<T> queryButBest(List<T> source)
    {
        var destination = new ArrayList<T>(startPoint.size() + 1);
        //
        for (int i = 1; i < startPoint.size() + 1; i++)
        {
            destination.add(source.get(i));
        }
        //
        return destination;
    }
    //
    private <T> T getBest(List<T> list)
    {
        return list.get(0);
    }
    //
    private <T> T getWorst(List<T> list)
    {
        return list.get(startPoint.size());
    }
    //
    private <T> T getSecondWorst(List<T> list)
    {
        return list.get(startPoint.size() - 1);
    }
}
