package chess.polytope;
//
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
//
final class ChessEvaluator implements Evaluator
{
    private static final Logger logger = Logger.getLogger("chess.polytope.ChessEvaluator");
    //
    private final long tries;
    //
    public ChessEvaluator(long tries)
    {
        super();
        //
        this.tries = tries;
    }
    //
    @Override
    public List<FunctionValue> evaluate(Collection<Vector> points)
    {
        var functionValues = new ArrayList<FunctionValue>(points.size());
        //
        for (var point : points)
        {
            functionValues.add(new FunctionValue(point));
        }
        //
        var games = new LinkedList<ChessGame>();
        //
        for (var functionValue1 : functionValues)
        {
            for (var functionValue2 : functionValues)
            {
                if (functionValue1 != functionValue2) games.add(new ChessGame(functionValue1, functionValue2, tries));
            }
        }
        //
        int gameNumber = 0;
        //
        for (var game : games)
        {
            gameNumber++;
            logger.info(String.format("game %s of %s", gameNumber, games.size()));
            game.play();
        }
        //
        for (var game : games)
        {
            game.adjustFunctionValues();
        }
        //
        Collections.sort(functionValues);
        //
        return functionValues;
    }
}
