package chess.polytope;
//
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
//
final class PolynomialEvaluator implements Evaluator
{
    private final double[] xs, ys;
    private int index;
    //
    public PolynomialEvaluator(int pointCount)
    {
        super();
        //
        xs = new double[pointCount];
        ys = new double[pointCount];
        index = 0;
    }
    //
    public void addPoint(double x, double y)
    {
        xs[index] = x;
        ys[index++] = y;
    }

    @Override
    public List<FunctionValue> evaluate(Collection<Vector> points)
    {
        var functionValues = new ArrayList<FunctionValue>(points.size());
        //
        for (var point : points)
        {
            functionValues.add(new FunctionValue(point, -deviation(point)));
        }
        //
        Collections.sort(functionValues);
        //
        return functionValues;
    }
    //
    private double deviation(Vector coefficients)
    {
        double deviation = 0d;
        //
        for (int i = 0; i < index; i++)
        {
            deviation += deviation(coefficients, xs[i], ys[i]);
        }
        //
        return Math.sqrt(deviation);
    }
    //
    private static double deviation(Vector coefficients, double x, double y)
    {
        double expected = 0d;
        //
        for (int i = 0; i < coefficients.size(); i++)
        {
            double coefficient = coefficients.get(i);
            expected *= x;
            expected += coefficient;
        }
        //
        double actual = y;
        //
        return square(actual - expected);
    }
    //
    private static double square(double value)
    {
        return value * value;
    }
}
