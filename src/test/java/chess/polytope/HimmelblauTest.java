package chess.polytope;
//
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
//
class HimmelblauTest
{
    @Test
    void testHimmelblau()
    {
        var evaluator = new HimmelblauEvaluator();
        var startPoint = new Vector(new double[] { -0.270845d, -0.923039d });
        var ascent = new PolytopeAscent(startPoint, evaluator);
        var result = ascent.findMaximum(1e-10d);
        Assertions.assertEquals(3.584428, result.get(0), 1e-3);
        Assertions.assertEquals(-1.84812, result.get(1), 1e-3);
    }
}
