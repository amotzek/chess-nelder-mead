package chess.polytope;
//
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
//
class RegressionTest
{
    @Test
    void testPolynomial2()
    {
        var evaluator = new PolynomialEvaluator(6);
        evaluator.addPoint(1d, 3d); // x*x + x + 1
        evaluator.addPoint(2d, 7d);
        evaluator.addPoint(3d, 13d);
        evaluator.addPoint(4d, 21d);
        evaluator.addPoint(5d, 31d);
        evaluator.addPoint(6d, 43d);
        var startPoint = new Vector(new double[] { 3d, 3d, 3d });
        var ascent = new PolytopeAscent(startPoint, evaluator);
        var result = ascent.findMaximum(1e-10d);
        Assertions.assertEquals(1d, result.get(0), 1e-3);
        Assertions.assertEquals(1d, result.get(1), 1e-3);
        Assertions.assertEquals(1d, result.get(2), 1e-3);
    }
    //
    @Test
    void testPolynomial3()
    {
        var evaluator = new PolynomialEvaluator(6);
        evaluator.addPoint(1d, 4d); // x*x*x + x*x + x + 1
        evaluator.addPoint(2d, 15d);
        evaluator.addPoint(3d, 40d);
        evaluator.addPoint(4d, 85d);
        evaluator.addPoint(5d, 156d);
        evaluator.addPoint(6d, 259d);
        var startPoint = new Vector(new double[] { 3d, 3d, 3d, 3d });
        var ascent = new PolytopeAscent(startPoint, evaluator);
        var result = ascent.findMaximum(1e-10d);
        Assertions.assertEquals(1d, result.get(0), 1e-3);
        Assertions.assertEquals(1d, result.get(1), 1e-3);
        Assertions.assertEquals(1d, result.get(2), 1e-3);
        Assertions.assertEquals(1d, result.get(3), 1e-3);
    }
}
