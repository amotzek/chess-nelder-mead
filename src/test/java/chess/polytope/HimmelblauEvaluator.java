package chess.polytope;
//
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
//
final class HimmelblauEvaluator implements Evaluator
{
    @Override
    public List<FunctionValue> evaluate(Collection<Vector> points)
    {
        var functionValues = new ArrayList<FunctionValue>(points.size());
        //
        for (var point : points)
        {
            double x = point.get(0);
            double y = point.get(1);
            double value = square(square(x) + y - 11) + square(x + square(y) - 7);
            var functionValue = new FunctionValue(point, -value);
            functionValues.add(functionValue);
        }
        //
        Collections.sort(functionValues);
        //
        return functionValues;
    }
    //
    private static double square(double s)
    {
        return s * s;
    }
}
